#include "tasksketchmanagerview.h"
#include "ui_form.h"

#include <QFileDialog>

TaskSketchManagerView::TaskSketchManagerView() :
	QObject(),
	PluginBase(this),
	ui(new Ui::Form),
	m_elementBase(new GUIElementBase(this, {"MainMenuItem"}))
{
	ui->setupUi(m_elementBase);

	initPluginBase({
		{INTERFACE(IPlugin), this},
		{INTERFACE(IGUIElement), m_elementBase}
	},
	{
		{INTERFACE(IUserTaskImageDataExtention), myModel}
	});
	m_elementBase->initGUIElementBase();

	connect(ui->buttonClose, SIGNAL(clicked(bool)), SLOT(buttonClose_clicked()));
	connect(ui->treeView, &QTreeView::entered, this, &TaskSketchManagerView::onTreeViewClicked);
	connect(ui->treeView, &QTreeView::activated, this, &TaskSketchManagerView::onTreeViewClicked);
	connect(ui->treeView, &QTreeView::clicked, this, &TaskSketchManagerView::onTreeViewClicked);
	connect(ui->treeView, &QTreeView::pressed, this, &TaskSketchManagerView::onTreeViewClicked);

	connect(ui->pushButtonAdd, &QPushButton::clicked, this, &TaskSketchManagerView::addImage);
	connect(ui->pushButtonDelete, &QPushButton::clicked, this, &TaskSketchManagerView::deleteImage);

	ui->graphicsView->setVisible(false);
	scene = new QGraphicsScene(this);
}

void TaskSketchManagerView::onReady()
{
	m_filterUserTasks = myModel->getModel()->getFilter();
	m_filterUserTasks->setColumns({
		{INTERFACE(IUserTaskDataExtention), {"name"}}
	});
	ui->treeView->setModel(m_filterUserTasks);
}

void TaskSketchManagerView::buttonClose_clicked()
{
	m_elementBase->closeSelf();
}

void TaskSketchManagerView::addImage()
{
	auto fileName = QFileDialog::getOpenFileName(m_elementBase, tr("Open Image"), "/home/", tr("Image Files (*.png *.jpg *.bmp)"));
	QImage newImage(fileName);

	QByteArray ba;
	QBuffer buffer(&ba);
	buffer.open(QIODevice::WriteOnly);
	newImage.save(&buffer, "PNG");
	buffer.close();

	auto data = myModel->getModel()->getItem(currentIndex);
	data[INTERFACE(IUserTaskImageDataExtention)]["image"] = ba;
	myModel->getModel()->updateItem(currentIndex, data);

	onTreeViewClicked(currentIndex);
}

void TaskSketchManagerView::deleteImage()
{
	auto data = myModel->getModel()->getItem(currentIndex);
	data[INTERFACE(IUserTaskImageDataExtention)]["image"] = QByteArray();
	myModel->getModel()->updateItem(currentIndex, data);

	onTreeViewClicked(currentIndex);
}

void TaskSketchManagerView::onTreeViewClicked(const QModelIndex &index)
{
	currentIndex = m_filterUserTasks->mapToSource(index);

	auto data = myModel->getModel()->getItem(currentIndex);
	auto imageData = data[INTERFACE(IUserTaskImageDataExtention)]["image"].toByteArray();

	bool isDataExists = imageData.length() != 0;
	ui->pushButtonAdd->setEnabled(!isDataExists);
	ui->pushButtonDelete->setEnabled(isDataExists);
	ui->graphicsView->setVisible(isDataExists);
	QPixmap pixmap;
	if(isDataExists)
	{
		auto image = QImage::fromData(imageData);
		pixmap = QPixmap::fromImage(image);
		scene->clear();
		scene->addPixmap(pixmap);
	}

	ui->graphicsView->setScene(scene);
}
