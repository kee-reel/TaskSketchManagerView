# Generated from TaskSketchManagerView.pro.

cmake_minimum_required(VERSION 3.16)
set( PROJECT_NAME TaskSketchManagerView )
project( ${PROJECT_NAME} )

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt6 COMPONENTS Core)
find_package(Qt6 COMPONENTS Gui)
find_package(Qt6 COMPONENTS Widgets)

qt_add_plugin( ${PROJECT_NAME} SHARED )
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "lib")
target_sources( ${PROJECT_NAME} PRIVATE
    ../../Interfaces/Architecture/GUIElementBase/../iguielement.h
    ../../Interfaces/Architecture/GUIElementBase/../referenceinstance.h
    ../../Interfaces/Architecture/GUIElementBase/../referenceinstanceslist.h
    ../../Interfaces/Architecture/GUIElementBase/../referenceshandler.h
    ../../Interfaces/Architecture/GUIElementBase/guielementbase.cpp ../../Interfaces/Architecture/GUIElementBase/guielementbase.h
    ../../Interfaces/Architecture/GUIElementBase/guielementlinkshandler.cpp ../../Interfaces/Architecture/GUIElementBase/guielementlinkshandler.h
    ../../Interfaces/Architecture/PluginBase/../iplugin.h
    ../../Interfaces/Architecture/PluginBase/../referenceinstance.h
    ../../Interfaces/Architecture/PluginBase/../referenceinstanceslist.h
    ../../Interfaces/Architecture/PluginBase/../referenceshandler.h
    ../../Interfaces/Architecture/PluginBase/plugin_base.cpp ../../Interfaces/Architecture/PluginBase/plugin_base.h
    ../../Interfaces/Architecture/PluginBase/plugindescriptor.h
    form.ui
    tasksketchmanagerview.cpp tasksketchmanagerview.h
    "res.qrc"
    "Res/ic_add_black_24dp.png"
    "Res/ic_cancel_black_24dp.png"
    "Res/ic_content_save_black_24dp.png"
    "Res/ic_delete_black_24dp.png"
    "Res/ic_exit_to_app_black_24dp.png"
    "Res/ic_file_restore_black_24dp.png"
    "Res/ic_image_multiple_black_24dp.png"
    "Res/sketch_to_task_24dp.png"
)
target_compile_definitions(TaskSketchManagerView PUBLIC
    QWidget_UIElement
)

target_link_libraries( ${PROJECT_NAME} PUBLIC
    Qt::Core
    Qt::Gui
    Qt::Widgets
)

set(SHARED_LIBRARY_NAME "${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}")

add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                "${CMAKE_CURRENT_BINARY_DIR}/${SHARED_LIBRARY_NAME}"
                "${CMAKE_CURRENT_BINARY_DIR}/../../../Application/Plugins/${SHARED_LIBRARY_NAME}")
